(function ($) {
    var endtime = new Date("06/10/2018 08:30:00");
    var currentime = new Date();

    var interval = (endtime.getTime() - currentime.getTime());

    var endDay = parseInt(interval/86400000);
    var endHour = parseInt((interval%86400000)/3600000);
    var endMin = parseInt(((interval%86400000)%3600000)/60000);
    var endSec = parseInt((((interval%86400000)%3600000)%60000)/1000);

    console.log('Endtime: ' + endDay + ' Days '
                            + endHour + ' Hours '
                            + endMin + ' Mins '
                            + endSec + ' Sec.');

    $('.cd100').countdown100({
        /*Set Endtime here*/
        /*Endtime must be > current time*/
        endtimeYear: 0,
        endtimeMonth: 0,
        endtimeDate: endDay,
        endtimeHours: endHour,
        endtimeMinutes: endMin,
        endtimeSeconds: endSec,
        timeZone: ""
        // ex:  timeZone: "America/New_York"
        //go to " http://momentjs.com/timezone/ " to get timezone
    });

})(jQuery);