<?php

namespace Drupal\coming_soon;

/**
 * Manages subscribers export.
 */
class SubscribersExporter {

  /**
   * Main "export" operation. (to be executed by the batch).
   */
  public static function export($count, &$context) {
    // Start working on a set of results.
    $limit = 50;
    $context['finished'] = 0;
    if (empty($context['sandbox']['offset'])) {
      $context['sandbox']['offset'] = 0;
    }

    $query = \Drupal::entityQuery('coming_soon_subscriber')
      ->range(
        $context['sandbox']['offset'],
        $limit
      )->sort('created');
    $nids = $query->execute();
    $nids = array_values($nids);

    $entity_storage = \Drupal::entityManager()->getStorage('coming_soon_subscriber');

    // Create the CSV file with the appropriate column headers for this
    // list/network if it hasn't been created yet, and store the file path and
    // field data in the $context for later retrieval.
    if (!isset($context['sandbox']['file'])) {

      // Get field names for this list/network. (I use a helper function here).
      $field_labels = ['ID', 'Email Address', 'Subscription date'];

      // Create the file and print the labels in the header row.
      $filename = 'list_subscribers_export.csv';
      $file_path = file_directory_temp() . '/' . $filename;
      // Create the file.
      $handle = fopen($file_path, 'w');
      // Write the labels to the header row.
      fputcsv($handle, $field_labels);
      fclose($handle);

      // Store file path and subscribers in $context.
      $context['sandbox']['file'] = $file_path;
      $context['sandbox']['subscribers_total'] = $count;

      // Store some values in the results array for processing when finshed.
      $context['results']['file'] = $file_path;
    }

    // Open the file for writing ('a' puts pointer at end of file).
    $handle = fopen($context['sandbox']['file'], 'a');

    // Loop until we hit the batch limit.
    for ($i = 0; $i < $limit; $i++) {
      $number_remaining = $context['sandbox']['subscribers_total'] - 1;
      $subscriber = $entity_storage->load($nids[$i]);
      if ($number_remaining && isset($nids[$i])) {
        $subscriber_data = [
          $subscriber->get('id')->getValue()[0]['value'],
          $subscriber->get('email')->getValue()[0]['value'],
          date('d-m-Y h:i:s', $subscriber->get('created')->getValue()[0]['value']),
        ];
        fputcsv($handle, $subscriber_data);

        // Increment the counter.
        $context['results']['count'][] = $subscriber->get('id')->getValue()[0]['value'];
        $context['finished'] = count($context['results']['count']) / $context['sandbox']['subscribers_total'];
      }
      // If there are no subscribers remaining, we're finished.
      else {
        $context['finished'] = 1;
        break;
      }
    }

    // Close the file.
    fclose($handle);
    // Increment iteration.
    $context['sandbox']['offset'] += $limit;

    // Show message updating user on how many subscribers have been exported.
    $context['message'] = t('Exported @count of @total subscribers.', [
      '@count' => count($context['results']['count']),
      '@total' => $context['sandbox']['subscribers_total'],
    ]);
  }

  /**
   * Finish exporting (batch) callback.
   */
  public function exportFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results['count']),
        'One subscriber exported susccessfully.', '@count subscribers exported susccessfully.'
      );
      drupal_set_message($message, 'status');
    }
    else {
      $message = t('There were errors during the export of this list.');
      drupal_set_message($message, 'warning');
    }

    // Set some session variables for the redirect to the file download page.
    $_SESSION['csv_download_file'] = $results['file'];
  }

}
